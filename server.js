var express = require('express');
var fs = require('fs');
var bodyParser = require('body-parser');

var server = express();
var jsonParser = bodyParser.json();

server.use(express.static(__dirname + '/public'));

server.get('/api/rows', function(req, res) {
  var content = fs.readFileSync('data.json', 'utf8');
  var rows = JSON.parse(content);
  res.send(rows);
})

server.get('/api/rows/:id', function(req, res) {
  var id = req.params.id;
  var content = fs.readFileSync('data.json', 'utf8');
  var rows = JSON.parse(content)[0];
  var row = null;
  for (var i = 0; i < rows.length; i++) {
    if (rows[i].id == id) {
      row = rows[i];
      break;
    }
  }
  if (row) {
    res.send(row);
  } else {
    res.status(404).send();
  }
})

server.post('/api/rows', jsonParser, function(req, res) {
  var firstName = req.body.first_name;
  var lastName = req.body.last_name;
  var email = req.body.email;
  var gender = req.body.gender;
  var ip = req.body.ip_address
  var row = {
    first_name: firstName,
    last_name: lastName,
    email: email,
    gender: gender,
    ip_address: ip
  };
  var content = fs.readFileSync("data.json", "utf8");
  var rows = JSON.parse(content)[0];
  var id = Math.max.apply(Math, rows.map(function(o) {
    return o.id;
  }))
  row.id = id + 1;
  rows.push(row);
  var jsonRows = [];
  jsonRows[0] = rows;
  var data = JSON.stringify(jsonRows);
  fs.writeFileSync("data.json", data);
  res.send(row);
})

server.delete("/api/rows/:id", function(req, res) {

  var id = req.params.id;
  //console.log(id);
  var content = fs.readFileSync('data.json', 'utf8');
  var rows = JSON.parse(content)[0];
  var row = null;
  for (var i = 0; i < rows.length; i++) {
    if (rows[i].id == id) {
      row = rows[i];
      break;
    }
  }
  if (row) {
    var targetRow = rows.splice(i, 1);
    //console.log(targetRow);
    var jsonRows = [];
    jsonRows[0] = rows;
    var data = JSON.stringify(jsonRows);
    fs.writeFileSync('data.json', data);

    res.send(targetRow);
  } else {
    res.status(404).send();
  }
})

server.put("/api/rows", jsonParser, function(req, res) {
  var id = req.body.id;
  var firstName = req.body.first_name;
  var lastName = req.body.last_name;
  var email = req.body.email;
  var gender = req.body.gender;
  var ip = req.body.ip_address
  var content = fs.readFileSync("data.json", "utf8");
  var rows = JSON.parse(content)[0];
  var row = null;

  for (var i = 0; i < rows.length; i++) {
    if (rows[i].id == id) {
      row = rows[i];
      break;
    }
  }
  if (row) {
    row.first_name = firstName;
    row.last_name = lastName;
    row.email = email;
    row.gender = gender;
    row.ip_address = ip;
    var jsonRows = [];
    jsonRows[0] = rows;
    var data = JSON.stringify(jsonRows);
    fs.writeFileSync("data.json", data);
    res.send(row);
  } else {
    res.status(404).send(rows);
  }
})

server.listen(3000, function() {
  console.log('shalom...');
})