var tbody = document.getElementsByTagName('tbody')[0];
var currentElem = null;

// tbody.onmousemove = function(e) {
//   console.log(e.target);
// }

tbody.onmouseover = function(e) {
  if (currentElem) {
    return;
  }
  var target = e.target;
  while (target != this) {
    if (target.tagName == 'TR') break;
    target = target.parentNode;
  }
  if (target == this) return;
  currentElem = target;
  var elems = target.querySelectorAll('.hide');
  for (var i = 0; i < elems.length; i++) {
    elems[i].classList.remove('hide');
    elems[i].classList.add('active');
  }
}

tbody.onmouseout = function(event) {
  if (!currentElem) return;
  var relatedTarget = event.relatedTarget;
  if (relatedTarget) {
    while (relatedTarget) {
      if (relatedTarget == currentElem) return;
      relatedTarget = relatedTarget.parentNode;
    }
  }
  var elems = currentElem.querySelectorAll('.active');
  for (var i = 0; i < elems.length; i++) {
    elems[i].classList.remove('active');
    elems[i].classList.add('hide');
  }
  currentElem = null;
};