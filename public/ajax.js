var tbody = document.getElementsByTagName('tbody')[0];
var form = document.getElementById('form');
var grid = document.getElementById('table');
var buttonReset = document.getElementById('buttonReset');
var buttonAdd = document.getElementById('buttonAdd');

function sortGrid(colNum, type) {
  var rowsArray = [].slice.call(tbody.rows);
  var compare;
  switch (type) {
    case 'number':
      compare = function(rowA, rowB) {
        return rowA.cells[colNum].innerHTML - rowB.cells[colNum].innerHTML;
      };
      break;
    case 'string':
      compare = function(rowA, rowB) {
        return (rowA.cells[colNum].innerHTML < rowB.cells[colNum].innerHTML) ? -1 : (rowA.cells[colNum].innerHTML > rowB.cells[colNum].innerHTML) ? 1 : 0;
      };
      break;
    case 'false':
      return;
  }
  rowsArray.sort(compare);
  grid.removeChild(tbody);
  for (var i = 0; i < rowsArray.length; i++) {
    tbody.appendChild(rowsArray[i]);
  }
  grid.appendChild(tbody);
}

function getRows() {
  var xhr = new XMLHttpRequest();
  xhr.open('get', '/api/rows');
  xhr.send();
  xhr.onload = function() {
    var data = JSON.parse(this.responseText);
    displayRows(data);
  }
}

function displayRows(data) {
  //console.log(data[0]);
  if (!data[0]) {
    var tr = document.createElement('tr');
    tr.innerHTML = row(data);
    tr.id = [data.id];
    tbody.appendChild(tr);
  } else {
    for (var i = 0; i < data[0].length; i++) {
      var userData = data[0][i];
      var tr = document.createElement('tr');
      tr.innerHTML = row(userData);
      tr.id = [userData.id];
      tbody.appendChild(tr);
    }
  }
}

function row(obj) {
  return `<td>${obj.id}</td><td>${obj.first_name}</td><td>${obj.last_name}</td><td>${obj.email}</td><td>${obj.gender}</td><td>${obj.ip_address}</td><td></td><td><img class="hide change" src="image/change.png"></td><td><img class="hide delete" src="image/delete.png"></td>`;
}

function getRow(id) {
  var xhr = new XMLHttpRequest();
  xhr.open('get', '/api/rows/' + id);
  xhr.send();
  xhr.onload = function() {
    var row = JSON.parse(this.responseText);
    var form = document.forms['list'];
    form.elements['id'].value = row.id;
    form.elements['first-name'].value = row.first_name;
    form.elements['last-name'].value = row.last_name;
    form.elements['email'].value = row.email;
    form.elements['gender'].value = row.gender;
    form.elements['ip-address'].value = row.ip_address;
  }
}

function createRow(id, firstName, lastName, email, gender, ipAddress) {
  var row = {
    id: id,
    first_name: firstName,
    last_name: lastName,
    email: email,
    gender: gender,
    ip_address: ipAddress
  }
  var json = JSON.stringify(row);
  var xhr = new XMLHttpRequest();
  xhr.open('POST', '/api/rows');
  xhr.setRequestHeader('Content-type', 'application/json; charset=utf-8');
  xhr.send(json);
  xhr.onload = function() {
    var data = JSON.parse(this.responseText);
    displayRows(data);
    resetForm();
  }
}

function deleteRow(id) {
  var xhr = new XMLHttpRequest();
  xhr.open('delete', '/api/rows/' + id);
  xhr.send();
  xhr.onload = function() {
    var data = JSON.parse(this.responseText);
    //console.log(data);
    var elem = document.getElementById(id);
    elem.remove();
  }
}

function editRow(id, firstName, lastName, email, gender, ipAddress) {
  var newRow = {
    id: id,
    first_name: firstName,
    last_name: lastName,
    email: email,
    gender: gender,
    ip_address: ipAddress
  }

  var json = JSON.stringify(newRow);
  var xhr = new XMLHttpRequest();
  xhr.open('put', '/api/rows');
  xhr.setRequestHeader('Content-type', 'application/json; charset=utf-8');
  xhr.send(json);
  xhr.onload = function() {
    var data = JSON.parse(this.responseText);
    var elem = document.getElementById(data.id);
    var tr = document.createElement('tr');
    tr.innerHTML = row(data);
    tr.id = [data.id];
    tbody.replaceChild(tr, elem);
    elem.remove();
    resetForm();
  }
}

document.body.onclick = function(e) {
  if (e.target.closest('.change')) {
    var id = e.target.closest('tr').getAttribute('id');
    getRow(id);
    form.classList.remove("hide");
    buttonAdd.classList.add("rotate");
  }
  if (e.target.closest('.delete')) {
    var id = e.target.closest('tr').getAttribute('id');
    deleteRow(id);
    resetForm();
  }
  if (e.target.tagName != 'TH') return;
  sortGrid(e.target.cellIndex, e.target.getAttribute('data-type'));
};

form.onsubmit = function(e) {
  e.preventDefault();
  var id = this.elements['id'].value;
  var firstName = this.elements['first-name'].value;
  var lastName = this.elements['last-name'].value;
  var email = this.elements['email'].value;
  var gender = this.elements['gender'].value;
  var ipAddress = this.elements['ip-address'].value;
  if (id == 0) {
    createRow(id, firstName, lastName, email, gender, ipAddress);
  } else {
    editRow(id, firstName, lastName, email, gender, ipAddress);
  }
}

buttonReset.onclick = function(e) {
  e.preventDefault();
  resetForm();
}

function showForm() {
  form.classList.toggle('hide');
  buttonAdd.classList.toggle('rotate');
  }

buttonAdd.onclick = function(e) {
  resetForm();
  showForm();
}

function resetForm() {
  form.reset();
  form.elements['id'].value = 0;
}

getRows();